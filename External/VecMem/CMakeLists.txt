# Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
#
# Package building VecMem for the offline builds.
#

# Set up the package:
atlas_subdir( VecMem )

# Silence ExternalProject warnings with CMake >=3.24.
if( POLICY CMP0135 )
   cmake_policy( SET CMP0135 NEW )
endif()

# Declare where to get VecMem from.
set( ATLAS_VECMEM_SOURCE
   "URL;http://cern.ch/atlas-software-dist-eos/externals/vecmem/v1.5.0.tar.gz;https://github.com/acts-project/vecmem/archive/refs/tags/v1.5.0.tar.gz;URL_MD5;3cc5a3bb14b93f611513535173a6be28"
   CACHE STRING "The source for VecMem" )
mark_as_advanced( ATLAS_VECMEM_SOURCE )

# Decide whether / how to patch the VecMem sources.
set( ATLAS_VECMEM_PATCH ""
   CACHE STRING "Patch command for VecMem" )
set( ATLAS_VECMEM_FORCEDOWNLOAD_MESSAGE
   "Forcing the re-download of VecMem (2023.12.12.)"
   CACHE STRING "Download message to update whenever patching changes" )
mark_as_advanced( ATLAS_VECMEM_PATCH ATLAS_VECMEM_FORCEDOWNLOAD_MESSAGE )

# Temporary directory for the build results.
set( _buildDir
   "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/VecMemBuild" )

# Extra arguments for the configuration.
set( _extraArgs )
if( NOT "${CMAKE_BUILD_TYPE}" STREQUAL "" )
   list( APPEND _extraArgs -DCMAKE_BUILD_TYPE:STRING=${CMAKE_BUILD_TYPE} )
endif()
if( NOT "${CMAKE_CXX_STANDARD}" STREQUAL "" )
   list( APPEND _extraArgs
      -DCMAKE_CXX_STANDARD:STRING=${CMAKE_CXX_STANDARD} )
endif()

# Find the optional external(s).
include( CheckLanguage )
check_language( CUDA )
set( ATLAS_VECMEM_USE_CUDA_DEFAULT FALSE )
if( CMAKE_CUDA_COMPILER )
   set( ATLAS_VECMEM_USE_CUDA_DEFAULT TRUE )
endif()
option( ATLAS_VECMEM_USE_CUDA
   "Use the CUDA capabilities of VecMem" ${ATLAS_VECMEM_USE_CUDA_DEFAULT} )
mark_as_advanced( ATLAS_VECMEM_USE_CUDA )
if( ATLAS_VECMEM_USE_CUDA )
   find_package( CUDAToolkit )
   if( CUDAToolkit_FOUND )
      message( STATUS "Building VecMem with CUDA support" )
      list( APPEND _extraArgs
         -DCUDAToolkit_ROOT:PATH=${CUDAToolkit_LIBRARY_ROOT}
         -DVECMEM_BUILD_CUDA_LIBRARY:BOOL=TRUE )
   endif()
endif()

# Build VecMem.
ExternalProject_Add( VecMem
   PREFIX "${CMAKE_BINARY_DIR}"
   INSTALL_DIR "${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}"
   ${ATLAS_VECMEM_SOURCE}
   ${ATLAS_VECMEM_PATCH}
   CMAKE_CACHE_ARGS -DCMAKE_INSTALL_PREFIX:PATH=${_buildDir}
   -DCMAKE_INSTALL_INCLUDEDIR:PATH=${CMAKE_INSTALL_INCLUDEDIR}/vecmem
   -DCMAKE_INSTALL_LIBDIR:PATH=${CMAKE_INSTALL_LIBDIR}
   -DVECMEM_BUILD_TESTING:BOOL=OFF
   -DVECMEM_BUILD_BENCHMARKING:BOOL=OFF
   ${_extraArgs}
   LOG_CONFIGURE 1 )
ExternalProject_Add_Step( VecMem forcedownload
   COMMAND ${CMAKE_COMMAND} -E echo
   "${ATLAS_VECMEM_FORCEDOWNLOAD_MESSAGE}"
   DEPENDERS download )
ExternalProject_Add_Step( VecMem purgeBuild
   COMMAND ${CMAKE_COMMAND} -E remove_directory "<BINARY_DIR>"
   COMMAND ${CMAKE_COMMAND} -E make_directory "<BINARY_DIR>"
   COMMAND ${CMAKE_COMMAND} -E remove_directory "${_buildDir}"
   COMMENT "Removing previous build results for VecMem."
   DEPENDEES download
   DEPENDERS patch )
ExternalProject_Add_Step( VecMem forceconfigure
   COMMAND ${CMAKE_COMMAND} -E remove -f "<BINARY_DIR>/CMakeCache.txt"
   COMMENT "Forcing the configuration of VecMem"
   DEPENDEES update
   DEPENDERS configure
   ALWAYS 1 )
ExternalProject_Add_Step( VecMem buildinstall
   COMMAND ${CMAKE_COMMAND} -E copy_directory "${_buildDir}/" "<INSTALL_DIR>"
   COMMENT "Installing VecMem into the build area"
   DEPENDEES install )
add_dependencies( Package_VecMem VecMem )

# Install VecMem.
install( DIRECTORY "${_buildDir}/"
   DESTINATION . USE_SOURCE_PERMISSIONS OPTIONAL )
install( FILES "cmake/Findvecmem.cmake"
   DESTINATION "${CMAKE_INSTALL_CMAKEDIR}/modules" )

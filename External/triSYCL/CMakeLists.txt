# Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration
#
# Package building triSYCL as part of the offline software build.
#

# Set the package name.
atlas_subdir( triSYCL )

# Stop if the build is not needed.
if( NOT ATLAS_BUILD_TRISYCL )
   return()
endif()

# Tell the user what's happening.
message( STATUS "Building triSYCL as part of this project" )

# Silence ExternalProject warnings with CMake >=3.24.
if( POLICY CMP0135 )
   cmake_policy( SET CMP0135 NEW )
endif()

# Find Boost, as it's needed by triSYCL.
find_package( Boost )
find_package( Rangev3 )

# Declare where to get triSYCL from.
set( ATLAS_TRISYCL_SOURCE
   "URL;http://cern.ch/atlas-software-dist-eos/externals/triSYCL/triSYCL-fbfc9c4d.tar.bz2;URL_MD5;32ff10134b962c2039aa7859125276af"
   CACHE STRING "The source for triSYCL" )
mark_as_advanced( ATLAS_TRISYCL_SOURCE )

# Decide whether / how to patch the triSYCL sources.
set( ATLAS_TRISYCL_PATCH "" CACHE STRING "Patch command for triSYCL" )
set( ATLAS_TRISYCL_FORCEDOWNLOAD_MESSAGE
   "Forcing the re-download of triSYCL (2022.10.11.)"
   CACHE STRING "Download message to update whenever patching changes" )
mark_as_advanced( ATLAS_TRISYCL_PATCH ATLAS_TRISYCL_FORCEDOWNLOAD_MESSAGE )

# Generate our own range-v3-config.cmake file, since the one provided by LCG is
# not functional. :-(
configure_file( "${CMAKE_CURRENT_SOURCE_DIR}/cmake/range-v3-config.cmake.in"
   "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/range-v3-config.cmake"
   @ONLY )

# The temporary directory to set up the built results in.
set( _buildDir
   "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/triSYCLBuild" )

# Set up the build of triSYCL for the build area.
ExternalProject_Add( triSYCL
   PREFIX "${CMAKE_BINARY_DIR}"
   INSTALL_DIR "${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}"
   ${ATLAS_TRISYCL_SOURCE}
   ${ATLAS_TRISYCL_PATCH}
   CMAKE_CACHE_ARGS -DCMAKE_BUILD_TYPE:STRING=${CMAKE_BUILD_TYPE}
   -DCMAKE_CXX_STANDARD:STRING=${CMAKE_CXX_STANDARD}
   -DCMAKE_INSTALL_PREFIX:PATH=${_buildDir}
   -DCMAKE_INSTALL_INCLUDEDIR:PATH=${CMAKE_INSTALL_INCLUDEDIR}/triSYCL
   -DBOOST_INCLUDEDIR:PATH=${Boost_INCLUDE_DIRS}
   -DBOOST_LIBRARYDIR:PATH=${Boost_LIBRARY_DIRS}
   -Drange-v3_DIR:PATH=${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}
   -DBUILD_TESTING:BOOL=FALSE
   -DTRISYCL_OPENMP:BOOL=FALSE -DTRISYCL_TBB:BOOL=FALSE
   -DTRISYCL_OPENCL:BOOL=FALSE
   LOG_CONFIGURE 1 )
ExternalProject_Add_Step( triSYCL forcedownload
   COMMAND ${CMAKE_COMMAND} -E echo "${ATLAS_TRISYCL_FORCEDOWNLOAD_MESSAGE}"
   DEPENDERS download )
ExternalProject_Add_Step( triSYCL purgeBuild
   COMMAND ${CMAKE_COMMAND} -E remove_directory "<BINARY_DIR>"
   COMMAND ${CMAKE_COMMAND} -E make_directory "<BINARY_DIR>"
   COMMAND ${CMAKE_COMMAND} -E remove_directory "${_buildDir}"
   COMMENT "Removing previous build results for triSYCL"
   DEPENDEES download
   DEPENDERS patch )
ExternalProject_Add_Step( triSYCL forceconfigure
   COMMAND ${CMAKE_COMMAND} -E remove -f "<BINARY_DIR>/CMakeCache.txt"
   COMMENT "Forcing the configuration of triSYCL"
   DEPENDEES update
   DEPENDERS configure
   ALWAYS 1 )
ExternalProject_Add_Step( triSYCL buildinstall
   COMMAND ${CMAKE_COMMAND} -E remove_directory "${_buildDir}/share"
   COMMAND ${CMAKE_COMMAND} -E copy_directory "${_buildDir}/" "<INSTALL_DIR>"
   COMMENT "Installing triSYCL into the build area"
   DEPENDEES install )
add_dependencies( Package_triSYCL triSYCL )

# Install triSYCL.
install( DIRECTORY "${_buildDir}/"
   DESTINATION . USE_SOURCE_PERMISSIONS OPTIONAL )

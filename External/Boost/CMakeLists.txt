# Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
#
# Configuration for building Boost as part of the offline / analysis release.
#

# Set the name of the package:
atlas_subdir( Boost )

# Stop if the build is not needed:
if( NOT ATLAS_BUILD_BOOST )
   return()
endif()

# Tell the user what's happening:
message( STATUS "Building Boost as part of this project" )

# Silence ExternalProject warnings with CMake >=3.24.
if( POLICY CMP0135 )
   cmake_policy( SET CMP0135 NEW )
endif()

# Declare where to get Boost from.
set( ATLAS_BOOST_SOURCE
   "URL;http://cern.ch/lcgpackages/tarFiles/sources/boost_1_86_0.tar.gz;URL_MD5;ac857d73bb754b718a039830b07b9624"
   CACHE STRING "The source for Boost" )
mark_as_advanced( ATLAS_BOOST_SOURCE )

# Decide whether / how to patch the Boost sources.
set( ATLAS_BOOST_PATCH "" CACHE STRING "Patch command for Boose" )
set( ATLAS_BOOST_FORCEDOWNLOAD_MESSAGE
   "Forcing the re-download of Boost (2023.07.26.)"
   CACHE STRING "Download message to update whenever patching changes" )
mark_as_advanced( ATLAS_BOOST_PATCH ATLAS_BOOST_FORCEDOWNLOAD_MESSAGE )

# Temporary directory for the build results:
set( _buildDir "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/BoostBuild" )

# Use all available cores for the build:
atlas_cpu_cores( nCPUs )

# Decide where to take Python from:
if( ATLAS_BUILD_PYTHON )
   set( Python_EXECUTABLE "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/python3" )
   set( Python_VERSION "3.11" )
   set( Python_INCLUDE_DIR "${CMAKE_INCLUDE_OUTPUT_DIRECTORY}/python3.11" )
   set( Python_LIBRARY_DIR "${CMAKE_LIBRARY_OUTPUT_DIRECTORY}" )
else()
   find_package( Python COMPONENTS Interpreter Development )
   list( GET Python_INCLUDE_DIRS 0 Python_INCLUDE_DIR )
   list( GET Python_LIBRARY_DIRS 0 Python_LIBRARY_DIR )
endif()
configure_file( "${CMAKE_CURRENT_SOURCE_DIR}/cmake/user-config.jam.in"
   "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/user-config.jam.pre"
   @ONLY )
file( GENERATE
   OUTPUT "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/user-config.jam"
   INPUT "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/user-config.jam.pre" )

# Only add debug symbols in Debug build mode:
set( _extraOpt )
if( "${CMAKE_BUILD_TYPE}" STREQUAL "Debug" )
   list( APPEND _extraOpt "variant=debug" )
else()
   list( APPEND _extraOpt "variant=release" )
endif()
# Set a compiler toolset explicitly. For the cases where multiple compiler
# types are available in the environment at the same time.
if( "${CMAKE_CXX_COMPILER_ID}" MATCHES "Clang" )
   list( APPEND _extraOpt "toolset=clang" )
elseif( "${CMAKE_CXX_COMPILER_ID}" MATCHES "GNU" )
   list( APPEND _extraOpt "toolset=gcc" )
endif()
# Set the C++ standard for the build explicitly.
if( "${CMAKE_CXX_STANDARD}" VERSION_GREATER_EQUAL "11" )
   list( APPEND _extraOpt "cxxflags=-std=c++${CMAKE_CXX_STANDARD}" )
endif()

# Create the helper scripts.
string( REPLACE ";" " " _configureOpt "${_extraOpt}" )
configure_file( "${CMAKE_CURRENT_SOURCE_DIR}/cmake/configure.sh.in"
   "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/configure.sh" @ONLY )
configure_file( "${CMAKE_CURRENT_SOURCE_DIR}/cmake/build.sh.in"
   "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/build.sh" @ONLY )
configure_file( "${CMAKE_CURRENT_SOURCE_DIR}/cmake/install.sh.in"
   "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/install.sh" @ONLY )

# Build Boost:
ExternalProject_Add( Boost
   PREFIX "${CMAKE_BINARY_DIR}"
   INSTALL_DIR "${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}"
   ${ATLAS_BOOST_SOURCE}
   ${ATLAS_BOOST_PATCH}
   BUILD_IN_SOURCE 1
   CONFIGURE_COMMAND
   "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/configure.sh"
   COMMAND ${CMAKE_COMMAND} -E copy
   "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/user-config.jam"
   "<BINARY_DIR>/tools/build/src/"
   BUILD_COMMAND
   "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/build.sh"
   INSTALL_COMMAND
   "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/install.sh"
   COMMAND ${CMAKE_COMMAND} -E copy_directory "${_buildDir}/"
   "<INSTALL_DIR>" )
ExternalProject_Add_Step( Boost forcedownload
   COMMAND ${CMAKE_COMMAND} -E echo "${ATLAS_BOOST_FORCEDOWNLOAD_MESSAGE}"
   DEPENDERS download )
add_dependencies( Package_Boost Boost )

# If we are building Python ourselves, make Boost depend on it:
if( ATLAS_BUILD_PYTHON )
   add_dependencies( Boost Python )
endif()

# Install Boost:
install( DIRECTORY "${_buildDir}/"
   DESTINATION . USE_SOURCE_PERMISSIONS OPTIONAL )

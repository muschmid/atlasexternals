# Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration

# The name of the package:
atlas_subdir( CheckerGccPlugins )

# Only do anything for a GCC >=8.
if( NOT CMAKE_COMPILER_IS_GNUCXX )
   return()
endif()
if( ${CMAKE_CXX_COMPILER_VERSION} VERSION_LESS 8 )
   return()
endif()

# External(s) needed by the compilation:
find_package( gmp )
if( NOT GMP_FOUND )
   message( WARNING "Could not find GMP, not building CheckerGccPlugins" )
   return()
endif()

# Check if the GCC development headers are available.
include( CheckIncludeFileCXX )
check_include_file_cxx( "gcc-plugin.h" GCC_PLUGIN_HEADER_FOUND "-iwithprefix plugin/include" )
if( NOT GCC_PLUGIN_HEADER_FOUND )
   message( WARNING "Could not find gcc-plugin.h, not building CheckerGccPlugins" )
   return()
endif()

# Disable the -Wl,--no-undefined linker flag for the library:
string( REPLACE "-Wl,--no-undefined" ""
   CMAKE_MODULE_LINKER_FLAGS "${CMAKE_MODULE_LINKER_FLAGS}" )

# Set up the build of the library:
atlas_add_library( checker_gccplugins
   src/*.cxx
   NO_PUBLIC_HEADERS MODULE
   PRIVATE_INCLUDE_DIRS ${GMP_INCLUDE_DIRS}
   PRIVATE_LINK_LIBRARIES ${GMP_LIBRARIES} )
target_compile_options( checker_gccplugins
   PRIVATE -fno-rtti -Wno-deprecated-copy )
target_compile_options( checker_gccplugins
   PRIVATE -iwithprefix plugin/include )


function( CheckerGccPlugins_test1 testName mode )
  cmake_parse_arguments( ARG "" "ENVIRONMENT" "" ${ARGN} )

  set( _fullName ${testName}_${mode} )
  if( "${mode}" STREQUAL "LTO" )
    set( _checkerTestFlags "${CMAKE_CXX_FLAGS_RELEASE} -flto " )
  else()
    set( _checkerTestFlags ${CMAKE_CXX_FLAGS_${mode}} )
  endif()
  configure_file( ${CMAKE_CURRENT_SOURCE_DIR}/test/checker_test.sh.in
                  ${CMAKE_CURRENT_BINARY_DIR}/CheckerGccPlugins_${_fullName}.sh
                  @ONLY )
  atlas_add_test( ${_fullName}
                  SCRIPT ${CMAKE_CURRENT_BINARY_DIR}/CheckerGccPlugins_${_fullName}.sh
                  ENVIRONMENT ${ARG_ENVIRONMENT}
                  POST_EXEC_SCRIPT "${CMAKE_CURRENT_SOURCE_DIR}/test/post.sh ${_fullName} '' ${CMAKE_CURRENT_SOURCE_DIR}/share/${testName}_test.ref"
                   )

endfunction( CheckerGccPlugins_test1 )


function( CheckerGccPlugins_test testName )
CheckerGccPlugins_test1( ${testName} RELEASE ${ARGN} )
CheckerGccPlugins_test1( ${testName} DEBUG ${ARGN} )
CheckerGccPlugins_test1( ${testName} LTO ${ARGN} )
endfunction( CheckerGccPlugins_test )


CheckerGccPlugins_test( gaudi_inheritance )
CheckerGccPlugins_test( divcheck )
CheckerGccPlugins_test( naming )
CheckerGccPlugins_test( check_ts1 )
CheckerGccPlugins_test( thread1 )
CheckerGccPlugins_test( thread2 )
CheckerGccPlugins_test( thread3 )
CheckerGccPlugins_test( thread4 )
CheckerGccPlugins_test( thread5 )
CheckerGccPlugins_test( thread6 )
CheckerGccPlugins_test( thread7 )
CheckerGccPlugins_test( thread8 )
CheckerGccPlugins_test( thread9 )
#CheckerGccPlugins_test( thread10 )
CheckerGccPlugins_test( thread11 )
CheckerGccPlugins_test( thread12 )
CheckerGccPlugins_test( thread13 )
CheckerGccPlugins_test( thread14 )
CheckerGccPlugins_test( thread15 )
CheckerGccPlugins_test( thread17 )
CheckerGccPlugins_test( thread18 )
CheckerGccPlugins_test( thread19 )
CheckerGccPlugins_test( thread20 )
CheckerGccPlugins_test( thread21 )
CheckerGccPlugins_test( thread22 )
CheckerGccPlugins_test( thread23 )
CheckerGccPlugins_test( thread24 )
CheckerGccPlugins_test( thread25 )
CheckerGccPlugins_test( thread26 )
CheckerGccPlugins_test( callcheck1 )
CheckerGccPlugins_test( callcheck2 )
CheckerGccPlugins_test( callcheck3 )
#CheckerGccPlugins_test( usingns1 )
#CheckerGccPlugins_test( usingns2 )
#CheckerGccPlugins_test( usingns3 )
#CheckerGccPlugins_test( usingns4 )
#CheckerGccPlugins_test( usingns5 )
#CheckerGccPlugins_test( usingns6 )
#CheckerGccPlugins_test( usingns7 )
#CheckerGccPlugins_test( usingns8 )
#CheckerGccPlugins_test( usingns9 )

CheckerGccPlugins_test( thread10 ENVIRONMENT CHECKERGCCPLUGINS_CONFIG=${CMAKE_CURRENT_SOURCE_DIR}/share/test_unchecked.config )
CheckerGccPlugins_test( thread16 ENVIRONMENT CHECKERGCCPLUGINS_CONFIG=${CMAKE_CURRENT_SOURCE_DIR}/share/test_unchecked.config )
CheckerGccPlugins_test( config ENVIRONMENT CHECKERGCCPLUGINS_CONFIG=${CMAKE_CURRENT_SOURCE_DIR}/share/test.config )
CheckerGccPlugins_test( check_ts2 ENVIRONMENT CHECKERGCCPLUGINS_CONFIG=${CMAKE_CURRENT_SOURCE_DIR}/share/test.config )


function( CheckerGccPlugins_ltolink_test )
  set( testName "ltolink" )
  set( _checkerTestFlags "${CMAKE_CXX_FLAGS_RELEASE} -flto " )
  configure_file( ${CMAKE_CURRENT_SOURCE_DIR}/test/checker_ltolink_test.sh.in
                  ${CMAKE_CURRENT_BINARY_DIR}/CheckerGccPlugins_ltolink.sh
                  @ONLY )
  atlas_add_test( ltolink
                  SCRIPT ${CMAKE_CURRENT_BINARY_DIR}/CheckerGccPlugins_ltolink.sh
                  ENVIRONMENT ${ARG_ENVIRONMENT}
                  POST_EXEC_SCRIPT "${CMAKE_CURRENT_SOURCE_DIR}/test/post.sh ltolink '' ${CMAKE_CURRENT_SOURCE_DIR}/share/ltolink_test.ref"
                   )
endfunction( CheckerGccPlugins_ltolink_test )

CheckerGccPlugins_ltolink_test()



CORAL - A DB Abstraction Layer With an SQL-free API
===================================================

This package builds CORAL for ATLAS offline software projects that do not depend
on tdaq-common, but do depend on CORAL.

# Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
#
# List of packages to build as part of VP1LightExternals.
#
+ External/Boost
+ External/Coin3D
+ External/Davix
+ External/dcap
+ External/Eigen
+ External/GeoModel
+ External/GoogleTest
+ External/LibXml2
+ External/nlohmann_json
+ External/Python
+ External/ROOT
+ External/Simage
+ External/SoQt
+ External/SQLite
+ External/TBB
+ External/XRootD
+ External/HepPDT
+ External/XercesC
- .*
